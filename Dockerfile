FROM alpine:3

RUN apk add asciinema
RUN apk add curl
RUN apk add git
RUN apk add fish

ADD https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install install_oh_my_fish
RUN fish install_oh_my_fish \
    --noninteractive

WORKDIR /home/user/Project/some-project

RUN git init && \
    git config user.name 'Your name' && \
    git config user.email 'your-email@domain.tld' && \
    git commit --allow-empty --message 'initial commit'

ENTRYPOINT [ "fish" ]
