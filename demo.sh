#!/usr/bin/env sh
set -e
podman build . --tag asciinema-omf
podman container run \
    --interactive \
    --tty \
    --rm \
    --volume "$PWD":/host-pwd \
    asciinema-omf

exit 0

echo "
asciinema rec /host-pwd/demo_oh_my_fish_pkg_enlarge_your_git_alias.cast

fish

git config alias.s status

type your alias then press space

s
=(

omf install enlarge_your_git_alias
s

=)
"
