<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

# enlarge your git alias
> A plugin for [Oh My Fish][omf].

[![Fish Shell Version][fish_version]][fish]
[![Oh My Fish Framework][omf_image]][omf]
[![Licence MIT][licence_mit_image]][mit]

<br/>

This plugin copies each [git-alias][git-alias] as abbreviations in [Fish Shell][fish]

## Demo

[![asciicast](https://asciinema.org/a/496261.svg)](https://asciinema.org/a/496261)


## Install

### Fisher

```fish
fisher install gitlab.com/pinage404/fish_plugin_package_enlarge_your_git_alias
```

### Oh My Fish

```fish
omf install enlarge_your_git_alias
```

## Usage

**Example:** If you have something like that in your `.gitconfig`


```ini
# ... something ...
[alias]
	u = "add --update"
	ci = "commit --verbose"
	f = "commit --amend --no-edit" # fixup the last commit
	sw = "switch"
	res = "restore"
	riom = "rebase --interactive origin/main"
	pr = "pull --rebase"
	flow = !sh -c "git-flow" # git flow is not an internal git command, we need to use the ! to specify a non-git command
# ... something ...
```


You can type the alias (then <kbd>Space</kbd>) to expand it to the full git command

```fish
u    # will be expanded to `git add --update"`
ci   # will be expanded to `git commit --verbose"`
f    # will be expanded to `git commit -C HEAD --amend"`
riom # will be expanded to `git rebase --interactive origin/master"`
pr   # will be expanded to `git pull --rebase"`
flow # will be expanded to `git-flow`
```


### Prefix

If you want to prefix the abbreviations with e.g. `g`

Add this to your Fish's configuration

```fish
set --export --global ENLARGE_YOUR_GIT_ALIAS_PREFIXED_WITH g
```

Before triggering Oh My Fish

```fish
source $OMF_PATH/init.fish
```

Then you will have this abbreviations

```fish
gu    # will be expanded to `git add --update"`
gci   # will be expanded to `git commit --verbose"`
gf    # will be expanded to `git commit -C HEAD --amend"`
griom # will be expanded to `git rebase --interactive origin/master"`
gpr   # will be expanded to `git pull --rebase"`
gflow # will be expanded to `git-flow`
```


## License

[MIT][mit] © [pinage404][author] et [al][contributors]


[fish]:              https://fishshell.com
[fish_version]:      https://img.shields.io/badge/fish-v2.2.0-007EC7.svg?style=flat-square

[omf]:               https://www.github.com/oh-my-fish/oh-my-fish
[omf_image]:         https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square

[mit]:               ./LICENSE.md
[licence_mit_image]: https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square

[git-alias]:         https://git-scm.com/book/tr/v2/Git-Basics-Git-Aliases

[author]:            https://gitlab.com/pinage404
[contributors]:      https://gitlab.com/pinage404/omf_pkg_enlarge_your_git_alias/graphs/master
