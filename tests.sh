#!/usr/bin/env sh
# run with
# watchexec --clear './tests.sh'

PACKAGE_NAME="enlarge-your-git-alias"
GIT_ALIAS_GIT_SUBCOMMAND="${PACKAGE_NAME}-git-subcommand"
GIT_ALIAS_SHELL_COMMAND="${PACKAGE_NAME}-shell-command"

fish_test_package() {
    CONFIG=""
    COMMAND=""
    if test -n "${2}"; then
        CONFIG="${1}"
        COMMAND="${2}"
    else
        CONFIG=""
        COMMAND="${1}"
    fi

    fish \
        --no-config \
        --private \
        --interactive \
        --init-command "
        ${CONFIG}
        source conf.d/__enlarge_your_git_alias_add_abbr.fish
        " \
        --command "${COMMAND}"
}

oneTimeSetUp() {
    if test -n "${GIT_DIR}"; then
        echo "GIT_DIR must be empty: ${GIT_DIR}"
        exit 1
    fi

    export GIT_DIR="${SHUNIT_TMPDIR}"
    git init
    git config "alias.${GIT_ALIAS_GIT_SUBCOMMAND}" 'commit --message WIP'
    git config "alias.${GIT_ALIAS_SHELL_COMMAND}" '!echo toto'
}

test_add_alias_as_abbr_exit_without_errors() {
    fish_test_package
}

test_add_alias_as_abbr() {
    fish_test_package "abbr --query ${GIT_ALIAS_GIT_SUBCOMMAND}"
}

test_add_alias_as_abbr_prefix_command_with_git() {
    ABBRS=$(fish_test_package "abbr --show")

    assertContains "${ABBRS}" "abbr -a -g -- ${GIT_ALIAS_GIT_SUBCOMMAND} 'git commit --message WIP'"
}

test_add_alias_as_abbr_shell_command_not_prefixed_with_git() {
    ABBRS=$(fish_test_package "abbr --show")

    assertContains "${ABBRS}" "abbr -a -g -- ${GIT_ALIAS_SHELL_COMMAND} 'echo toto'"
}

test_add_alias_as_prefixed_abbr() {
    ABBRS=$(fish_test_package "set --export --global ENLARGE_YOUR_GIT_ALIAS_PREFIXED_WITH prefix" "abbr --show")

    assertContains "${ABBRS}" "abbr -a -g -- prefix${GIT_ALIAS_SHELL_COMMAND} 'echo toto'"
}

. shunit2
